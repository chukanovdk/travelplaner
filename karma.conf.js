const istanbul = require('rollup-plugin-istanbul');

module.exports = function(config) {
    config.set({
        coverageReporter: {
            dir:'tmp/coverage/',
            reporters: [
                { type:'html', subdir: 'report-html' },
            ],
            instrumenterOptions: {
                istanbul: { noCompact:true }
            }
        },
        files: [
            { pattern: 'src/**/*.spec.js', watched: false },
        ],

        preprocessors: {
            'src/**/*.spec.js': ['rollup'],
        },
        singleRun: true,
        browsers: ['Chrome'],
        frameworks:  [ 'mocha','sinon','chai'],
        reporters: ['mocha', 'coverage'],
        rollupPreprocessor: {
            plugins: [istanbul({
                exclude: ['src/**/*.spec.js']
            })],
            output: {
                format: 'iife',
                name: 'Test',
                sourcemap: 'inline',
            },
        },
    })
}
