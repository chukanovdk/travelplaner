// rollup.config.js
import browsersync from 'rollup-plugin-browsersync';
import htmlTemplate from 'rollup-plugin-generate-html-template';
import nodeResolve from 'rollup-plugin-node-resolve';
import commonJs from 'rollup-plugin-commonjs';
import {sizeSnapshot} from "rollup-plugin-size-snapshot";
import {terser} from 'rollup-plugin-terser';
import babel from 'rollup-plugin-babel';
import istanbul from 'rollup-plugin-istanbul';
import livereload from 'rollup-plugin-livereload'
const isProduction = NODE_ENV => NODE_ENV ==="production";

const buildPlugin = [
    babel({
        exclude: 'node_modules/**'
    }),
    nodeResolve(),
    commonJs(),
    sizeSnapshot(),
    terser(),
    htmlTemplate({
        template: 'src/template.html',
        target: 'index.html',
    }),
];

const devPlugin = [
    istanbul([
        'src/**/*.spec.js'
    ]),
    babel({
        exclude: ['node_modules/**']
    }),
    nodeResolve(),
    commonJs(),
    sizeSnapshot(),
    htmlTemplate({
        template: 'src/template.html',
        target: 'index.html',
    }),
    browsersync({server: 'dist',}),
    livereload()
];



export default {
    input: 'src/travelPlanner.js',
    output: [
        {
            file: 'dist/bundle.js',
            format: 'iife',
        },
    ],
    plugins: isProduction(process.env.NODE_ENV) ? buildPlugin : devPlugin
}