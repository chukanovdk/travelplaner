# Сортировщик карточек путешественника

Вам дана стопка посадочных карточек на различные виды транспорта, которые доставят вас из точки A в точку B. Карточки перепутаны, и вы не знаете, где начинается и где заканчивается ваше путешествие. Каждая карточка содержит информацию о том, откуда и куда вы едете на данном отрезке маршрута, а также о типе транспорта (номер рейса, номер места и прочее).

Предоставьте JavaScript API, который отсортирует такой список карточек и вернет словесное описание, как проделать ваше путешествие. API должен принимать на вход несортированный список карточек в формате, придуманном вами, и возвращать, например, такое описание:

* Take train 78A from Madrid to Barcelona. Seat 45B.
* Take the airport bus from Barcelona to Gerona Airport. No seat assignment.
* From Gerona Airport, take flight SK455 to Stockholm. Gate 45B. Seat 3A. Baggage drop at ticket counter 344.
* From Stockholm, take flight SK22 to New York JFK. Gate 22. Seat 7B. Baggage will be automatically transferred from your last leg.

### Требования:

* Алгоритм должен работать с любым количеством карточек, если все карточки образуют одну неразрывную цепочку.
* Время прибытия и отправления неизвестно и неважно. Подразумевается, что средство передвижения для следующего отрезка дожидается вас.
* Структура кода должна быть расширяема для использования любых типов транспорта и информации, которая может быть связана с каждым типом транспорта.
* API будет вызываться из других частей JavaScript-кода без необходимости дополнительных запросов между браузером и сервером.
* Не используйте библиотеки и фреймворки, напишите все с нуля.
* Задокументируйте в коде формат входных и выходных данных.

### Что нас интересует:
* Какой формат входных данных вы придумаете.
* Как вы структурируете свой код, чтобы он был расширяем, легок к пониманию и изменениям другими программистами.
* Какой алгоритм сортировки вы придумаете.

===============

# Решение

## Установка

```
npm i
npm run test
npm run build
```

## Документация

### Формат входных данных 
```
/**
 @card ->
 *   departure: String, represents the place where it starts
 *   destination: String, represents the place where it finishes
 *   transport_type: String, represents the type of the transport
 *   otherInformation -> (Not necessary)
 *     route: String, represents the number of the route
 *     gate: String, represents the number of the gate
 *     seat: String, represents the number of the seat
 *     baggage_drop: String, represents where to find the baggage
 *     [any key]: String, represents any additional stuff, not mentioned
 */

[
{
    departure:'Stockholm',
    destination:'New York JFK',
    transport_type:'plane',
    otherInformation: {
      route: 'SK455',
      gate: '22',
      seat: '7B',
      baggage_drop: '334',
      notes: 'Baggage will be automatically transferred from your last leg'
    }
  }, ...
]
```