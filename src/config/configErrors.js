
export const $errors = {
    noTrip: {
        cards: `No any travel cards provided to the constructor. \n Please, provide some travel cards.`
    },
    invalidCard: {
        common: `The following card is invalid: \n`,
        noFrom: 'Missing starting location. Please, provide starting location [departure]',
        noTo: 'Missing destination place. Please, provide destination place [destination]',
        noTransport: 'Missing transport type. Please, provide transport type [transport_type]'
    }
};
