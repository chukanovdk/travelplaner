/**
 * Traveler Card Sorter
 * Author: Chukanov Dmitry
 */

import { $errors } from "./config/configErrors";
import { validateCards } from "./components/validateCards/validateCards";
import { buildHashTableCards } from "./components/buildHashTableCards/buildHashTableCards";
import {compose, log} from "./helpers/helpers";
import { initialSearchCard } from "./components/initialSearchCards/initialSearchCards";
import { sortingCards } from "./components/sortingCards/sortingCards";
import {renderCards} from "./components/renderCards/renderCards";

export const travelPlanner = cards => {
  if (!cards) {
    throw new Error($errors.noTrip.cards);
  }

  const runPlanner = () =>
    compose(
       log,
      renderCards,
      sortingCards(cards, buildHashTableCards(cards)),
      initialSearchCard,
      validateCards
    )(cards);

  return {
    runPlanner,
  };
};

window.travelPlanner = travelPlanner;
