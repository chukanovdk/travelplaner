/**
 *  Search initial card.
 *
    Creates an array with all destinations and then determines
     point of departure, which is not present in this array.
   * Time complexity - O (2n) = O (n).
 * @param {[]} cards
 * @returns {[]}
 */
export const initialSearchCard = cards => {
  const destination = new Map();
  const preSortedCards = [];

  cards.forEach( card => destination.set(card.destination, card));

  for (const card of cards) {
    if (destination.has(card.departure)) continue;
    preSortedCards.push(card);
    break;
  }
  return preSortedCards;
};
