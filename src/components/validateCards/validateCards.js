import { $errors } from "../../config/configErrors";

/**
 *
 * @param {Array} cards
 */
export const validateCards = (cards) =>
  cards.map((card) => {
    let _error = null;

    if (!card.departure) _error = "noFrom";
    else if (!card.destination) _error = "noTo";
    else if (!card.transport_type) _error = "noTransport";

    if (_error) {
      throw new Error(
        `${$errors.invalidCard.common}${JSON.stringify(card, {}, 4)} \n ${
          $errors.invalidCard[_error]
        }`
      );
    }
    return card;
  });
