/**
 *  Provides quick access to the next card in the route.
 * @param cards
 * @returns {Map<String, Number>}
 */
export const buildHashTableCards = cards => {
  const hashTableCards = new Map();
  cards.forEach((card, id) => hashTableCards.set(card.departure, id));
  return hashTableCards;
};
