/**
 *
 * Sort cards / route planning

* Knowing the first card and having access to the hash table,
       building a route becomes trivial. 
* 1) Find the value of card.destination in the hash table.
* 2) In the initial array of cards find the next card cards [index].
* Time complexity - O (1) * n = O (n).
 * @param {[]} cards
 * @param {{}} hashTableCards
 * @returns {function([]): []}
 */
export const sortingCards = (cards, hashTableCards) => (preSortedArr) => {
  const saveLength = cards.length - 1;
  const sorted = [...preSortedArr];
  for (let i = 0; i < saveLength; i++) {
    const currentCard = sorted[i];
    const nextCardIndex = hashTableCards.get(currentCard.destination);
    const nextCard = cards[nextCardIndex];
    sorted.push(nextCard);
  }
  return sorted;
};
