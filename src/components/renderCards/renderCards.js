/**
 *
 * @param {[]} sorted
 * @returns {HTMLDivElement}
 */
export const renderCards = sorted => {
/** Creates an item to display the route. */
    const itinerary = document.createElement('div');
    const  renderSeat = card => card.otherInformation.seat ? `Seat ${card.otherInformation.seat}` : 'No seat assigned.';
    const  choiceTranspotType = card => {
        let cardDirection = '';
        let transportType = {
            'plane' :() => {
                let baggage = (card.otherInformation.baggage_drop ? `Baggage drop at ticket counter ${card.otherInformation.baggage_drop}.` : '');
                cardDirection =`<span>From ${card.departure}, take flight ${card.otherInformation.route} to ${card.destination}. Gate ${card.otherInformation.gate}. ${renderSeat(card)} ${baggage} ${card.otherInformation.notes || ''}</span><br>`;
            },
            'train':() => {
                cardDirection = `<span>Take train ${card.otherInformation.route} from ${card.departure} to ${card.destination}. ${renderSeat(card)} ${card.otherInformation.notes || ''}</span><br>`;
            },
            'airport_bus': ()=> {
                cardDirection =`<span>Take the airport bus from ${card.departure} to ${card.destination}. ${renderSeat(card)} ${card.otherInformation.notes || ''}</span><br>`;
            },
            'taxi': () => {
                cardDirection =`<span>Take a Taxi from ${card.departure} to ${card.destination}. ${card.otherInformation.notes || ''}</span><br>`;
            },
            'walking':() =>{
                cardDirection =`<span>Walk from ${card.departure} to ${card.destination}. ${card.otherInformation.notes || ''}</span><br>`;
            } ,
            'default':() =>{
                cardDirection =`<span>Go from ${card.departure} to ${card.destination}. ${card.otherInformation.notes || ''}</span><br>`;
            }
        };

        (transportType[card.transport_type] || transportType['default'])();
//Inserts directions into the element with id 'itinerary'.
        return  itinerary.innerHTML += cardDirection;
    };
    sorted.forEach(choiceTranspotType);
    return document.getElementsByTagName('body')[0].appendChild(itinerary);
};