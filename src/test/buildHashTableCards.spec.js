import { initialSearchCard } from "../components/initialSearchCards/initialSearchCards";

import { CARDS } from "./data/cards";
import { PRE_SORTED_ARR } from "./data/preSortedArr";
import { buildHashTableCards } from "../components/buildHashTableCards/buildHashTableCards";
import { convertMapToObject } from "../helpers/helpers";
import {HASH_TABLE_CARDS} from "./data/hashTableCards";

describe("#buildHashTableCards()", () => {
  it("buildHashTableCards", () => {
    assert.equal(
      JSON.stringify(convertMapToObject(buildHashTableCards(CARDS))),
      JSON.stringify(HASH_TABLE_CARDS)
    );
  });
});
