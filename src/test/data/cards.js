export const CARDS = [
    {
        departure:'Barcelona',
        destination: 'Gerona Airport',
        transport_type:'airport_bus',
        otherInformation: {
            route: '',
            gate: '',
            seat: '',
            baggage_drop: '',
            notes: ''
        }
    },
    {
        departure:'Stockholm',
        destination:'New York JFK',
        transport_type:'plane',
        otherInformation: {
            route: 'SK455',
            gate: '22',
            seat: '7B',
            baggage_drop: '',
            notes: 'Baggage will be automatically transferred from your last leg'
        }
    },
    {
        departure:'Madrid',
        destination:'Barcelona',
        transport_type: 'train',
        otherInformation: {
            route: '78A',
            gate: '',
            seat: '45B',
            baggage_drop: '',
            notes: ''
        }
    },
    {
        departure:'Gerona Airport',
        destination:'Stockholm',
        transport_type: 'plane',
        otherInformation:  {
            route: 'SK455',
            gate: '45B',
            seat: '3A',
            baggage_drop: '334',
            notes: ''
        }
    }
];