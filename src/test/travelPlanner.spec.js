import { travelPlanner } from "../travelPlanner.js";
import { CARDS } from "./data/cards";

describe("#travelPlanner()", () => {
  it("travelPlanner", () => {
    const testDivResult = document.createElement("div");
    testDivResult.innerHTML = `<span>Take train 78A from Madrid to Barcelona. Seat 45B </span><br><span>Take the airport bus from Barcelona to Gerona Airport. No seat assigned. </span><br><span>From Gerona Airport, take flight SK455 to Stockholm. Gate 45B. Seat 3A Baggage drop at ticket counter 334. </span><br><span>From Stockholm, take flight SK455 to New York JFK. Gate 22. Seat 7B  Baggage will be automatically transferred from your last leg</span><br>`;
    assert.equal(
      travelPlanner(CARDS).runPlanner().innerHTML,
      testDivResult.innerHTML
    );
  });
});
