import { CARDS } from "./data/cards";
import { sortingCards } from "../components/sortingCards/sortingCards";
import { HASH_TABLE_CARDS } from "./data/hashTableCards";
import { SORTED_CARDS } from "./data/sortedCards";
import { PRE_SORTED_ARR } from "./data/preSortedArr";
import { convertObjectToMap } from "../helpers/helpers";

describe("#sortingCards()", () => {
  it("sortingCards", () => {
    assert.equal(
      JSON.stringify(
        sortingCards(
          CARDS,
          convertObjectToMap(HASH_TABLE_CARDS)
        )(PRE_SORTED_ARR)
      ),
      JSON.stringify(SORTED_CARDS)
    );
  });
});
