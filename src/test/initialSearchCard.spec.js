import { initialSearchCard } from "../components/initialSearchCards/initialSearchCards";

import { CARDS } from "./data/cards";
import { PRE_SORTED_ARR } from "./data/preSortedArr";

describe("#initialSearchCard()", () => {
  it("initialSearchCard", () => {
    assert.equal(
      JSON.stringify(initialSearchCard(CARDS)),
      JSON.stringify(PRE_SORTED_ARR)
    );
  });
});
