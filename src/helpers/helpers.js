/**
 * Композиция функций
 * @param  {...function} functions
 */
export const compose = (...functions) => functions.reduce((f, g) => (...args) => f(g(...args)));

/**
 *
 * @param value
 */
export const log = value => ( console.log(value), value );

/**
 *
 * @param {{}} obj
 * @returns {Map<string, unknown>}
 */
export const convertObjectToMap  = obj => new Map(Object.entries(obj));

/**
 *
 * @param {Map} map
 * @returns {any}
 */
export const convertMapToObject = map => Object.fromEntries(map);